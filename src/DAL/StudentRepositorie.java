/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL;

import DTL.Student;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author AlbertFields
 */
public class StudentRepositorie {
        final static String USER ="DESKTOP-5GUJB6U";
        final static int PORT = 1433;
        final static String DB_NAME ="Comedor";
        
        
  private static ArrayList<Student> getDataByQuery(String query) {
        ArrayList<Student> oStudent = new ArrayList<>();
        
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                Student S = new Student();
                S.setStudentID(rs.getInt("StudentID"));
                S.setFirstName(rs.getString("FirstName"));
                S.setLastName(rs.getString("LastName"));
                S.setMajor(rs.getString("Major"));
                S.setStudentStatus(rs.getBoolean("StudentStatus"));

                oStudent.add(S);
                index++;
            }
            rs.close();

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oStudent;
    }

    private static void executeQuery(String query) {

        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName(USER);
            //ds.setInstanceName(query);
            ds.setPortNumber(PORT);
            ds.setDatabaseName(DB_NAME);
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<Student> getAllStudents() {
        String query = "SELECT * FROM Students";
        ArrayList<Student> oStudents= getDataByQuery(query);
        return oStudents;
    }

    public static ArrayList<Student> getStudentByID(String id) {
        String query = "SELECT  * "
                + "FROM Students "
                + "WHERE StudentID =  " + id;
        ArrayList<Student> oStudent= getDataByQuery(query);
        return oStudent;
    }
    
       public static void createFine(Student student){
        String query = " INSERT INTO Students (StudentID, FirstName, LastName, Major, StudentStatus) " 
                + "VALUES ('"+student.getStudentID()+"','"+ student.getFirstName() +","+student.getLastName() +","+student.getMajor() +","+student.getStudentStatus()+"')";
        executeQuery(query);
    }

}
