/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class UserType {
    private int userTypeID;
    private String userName;

    public UserType(int _userTypeID, String _userName) {
        this.userTypeID = _userTypeID;
        this.userName = _userName;
    }
    public UserType(){
        
    }
    public int getUserTypeID() {
        return userTypeID;
    }

    public void setUserTypeID(int _userTypeID) {
        this.userTypeID = _userTypeID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String _userName) {
        this.userName = _userName;
    }   
}
