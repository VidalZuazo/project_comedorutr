/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

import java.util.Date;

/**
 *
 * @author AlbertFields
 */
public class Fine {

    private int fineID;
    private boolean fineStatus;
    private java.util.Date fineDate;
    private java.util.Date fineDeadline;
    private int studentID;
    private int fineTypeID;

    
    public Fine(){
        
    }

    public Fine(int _fineID, boolean _fineStatus, Date _fineDate, Date _fineDeadline, int _studentID, int _fineTypeID) {
        this.fineID = _fineID;
        this.fineStatus = _fineStatus;
        this.fineDate = _fineDate;
        this.fineDeadline = _fineDeadline;
        this.studentID = _studentID;
        this.fineTypeID = _fineTypeID;
    }

    public int getFineID() {
        return fineID;
    }

    public void setFineID(int fineID) {
        this.fineID = fineID;
    }

    public boolean getFineStatus() {
        return fineStatus;
    }

    public void setFineStatus(boolean fineStatus) {
        this.fineStatus = fineStatus;
    }

    public Date getFineDate() {
        return fineDate;
    }

    public void setFineDate(Date fineDate) {
        this.fineDate = fineDate;
    }

    public Date getFineDeadline() {
        return fineDeadline;
    }

    public void setFineDeadline(Date fineDeadline) {
        this.fineDeadline = fineDeadline;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public int getFineTypeID() {
        return fineTypeID;
    }

    public void setFineTypeID(int fineTypeID) {
        this.fineTypeID = fineTypeID;
    }
    
    
}