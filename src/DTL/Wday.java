/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class Wday {
    
    private int WdayID;
    private int studentID;
    private boolean monday;
    private boolean tuesday;
    private boolean wednesday;
    private boolean thursday;
    private boolean friday;
    
    public Wday(){

    }

    public Wday(int _wdayID,int _studentID, boolean _monday, boolean _tuesday, boolean _wednesday, boolean _thursday, boolean _friday) {
        this.WdayID = _wdayID;
        this.studentID = _studentID;
        this.monday = _monday;
        this.tuesday = _tuesday;
        this.wednesday = _wednesday;
        this.thursday = _thursday;
        this.friday = _friday;
    }

    public int getWdayID() {
        return WdayID;
    }

    public void setWdayID(int WdayID) {
        this.WdayID = WdayID;
    }

    
    
    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public boolean isMonday() {
        return monday;
    }

    public void setMonday(boolean monday) {
        this.monday = monday;
    }

    public boolean isTuesday() {
        return tuesday;
    }

    public void setTuesday(boolean tuesday) {
        this.tuesday = tuesday;
    }

    public boolean isWednesday() {
        return wednesday;
    }

    public void setWednesday(boolean wednesday) {
        this.wednesday = wednesday;
    }

    public boolean isThursday() {
        return thursday;
    }

    public void setThursday(boolean thursday) {
        this.thursday = thursday;
    }

    public boolean isFriday() {
        return friday;
    }

    public void setFriday(boolean friday) {
        this.friday = friday;
    }

}

