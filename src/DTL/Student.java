/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL;

/**
 *
 * @author AlbertFields
 */
public class Student {

    private int StudentID;
    private String firstName;
    private String lastName;
    private String major;
    private boolean studentStatus;

    public Student() {
    }

    public Student(int _studentID, String _firstName, String _lastName, String _major, boolean _studentStatus) {
        this.StudentID = _studentID;
        this.firstName = _firstName;
        this.lastName = _lastName;
        this.major = _major;
        this.studentStatus = _studentStatus;
    }

    public int getStudentID() {
        return StudentID;
    }

    public void setStudentID(int studentID) {
        this.StudentID = studentID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    
    
    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public boolean getStudentStatus() {
        return studentStatus;
    }

    public void setStudentStatus(boolean studentStatus) {
        this.studentStatus = studentStatus;
    }
    
    

 
}
