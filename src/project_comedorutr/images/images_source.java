/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_comedorutr.images;

import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;
//Log In
import static project_comedorutr.interfaces.interface_login.background_login;
import static project_comedorutr.interfaces.interface_login.label_password;
import static project_comedorutr.interfaces.interface_login.label_user;
import static project_comedorutr.interfaces.interface_administrator.label_logo_administrator_one;
import static project_comedorutr.interfaces.interface_administrator_add.label_logo_administrator_add;
import static project_comedorutr.interfaces.interface_administrator_delete.label_logo_administrator_delete;
import static project_comedorutr.interfaces.interface_administrator_update.label_logo_administrator_update;
import static project_comedorutr.interfaces.interface_cashier.label_logo_cashier;
import static project_comedorutr.interfaces.interface_dinningroom.label_logo_dinningroom;
/**
 *
 * @author Pavilion
 */
public class images_source {

    //Log In Images
    public void background_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/comedor_escolar_18-01.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(background_login.getWidth(), background_login.getHeight(), Image.SCALE_DEFAULT));
        background_login.setIcon(i);
    }
    
    public void user_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/user.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_user.getWidth(), label_user.getHeight(), Image.SCALE_DEFAULT));
        label_user.setIcon(i);
    }
    
    public void password_login() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/lock.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_password.getWidth(), label_password.getHeight(), Image.SCALE_DEFAULT));
        label_password.setIcon(i);
    }
    
    //Administrator Images
    
    public void logo_administrator_main() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_one.getWidth(), label_logo_administrator_one.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_one.setIcon(i);
    }
    
    public void logo_administrator_add() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_add.getWidth(), label_logo_administrator_add.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_add.setIcon(i);
    }
    
    public void logo_administrator_delete() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_delete.getWidth(), label_logo_administrator_delete.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_delete.setIcon(i);
    }
    
    public void logo_administrator_update() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_administrator_update.getWidth(), label_logo_administrator_update.getHeight(), Image.SCALE_DEFAULT));
        label_logo_administrator_update.setIcon(i);
    }
    
    //Cashier Images
    
    public void logo_cashier() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_cashier.getWidth(), label_logo_cashier.getHeight(), Image.SCALE_DEFAULT));
        label_logo_cashier.setIcon(i);
    }
    
    //Dinningroom Images
    
    public void logo_dinningroom() {
        ImageIcon img = new ImageIcon("src/project_comedorutr/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(label_logo_dinningroom.getWidth(), label_logo_dinningroom.getHeight(), Image.SCALE_DEFAULT));
        label_logo_dinningroom.setIcon(i);
    }

}
