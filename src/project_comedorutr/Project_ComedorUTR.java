/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project_comedorutr;


import DAL.*;
import DTL.*;

import java.util.ArrayList;


/**
 *
 * @author Pavilion
 */
public class Project_ComedorUTR {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //interface_login run = new interface_login();
        //interface_administrator run = new interface_administrator();
        //interface_administrator_add run = new interface_administrator_add();
        //interface_cashier run = new interface_cashier();
        //interface_dinningroom run = new interface_dinningroom();
        //run.setVisible(true);

        
        
        // creo un arraylist y lo lleno con todas las multas usando el metodo getAllFines();
        ArrayList<Fine> fines = FineRepositorie.getAllFines();
        
        //para poder acceder a cada parametro del tipo fine vamos a recorrer el arraylist
        for(int i = 0; i<fines.size();i++){
            //y crearemos un objeto de tipo fine que ira tomando los valores del arraylist en el indice i
            Fine fine = fines.get(i);
            // el objeto de tipo Fine puede acceder a sus metodos y asi obtener los parametros necesarios
            System.out.println(fine.getStudentID());
            System.out.println(fine.getFineStatus());
            System.out.println(fine.getFineDate());
            System.out.println(fine.getFineDeadline());
            System.out.println(fine.getFineTypeID());

        }

        
    }
    
}
